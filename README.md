# Objetivo

O objetivo deste projeto é de realizar vários testes de machine learning com diferentes datasets de saúde, utilizando diferentes técnicas.

A base de conhecimento técnico de Machine Learning eu tenho adquirido através do curso da Udemy (https://www.udemy.com/machine-learning-e-data-science-com-python-y). Além do mais, dedico todo esse trabalho ao meu amigo Álvaro, que tem me auxiliado muito (https://gitlab.com/Alvarole)

---

## Datasets

### OPima Indians Diabetes Database

* Referência: https://www.kaggle.com/uciml/pima-indians-diabetes-database#diabetes.csv


